/* SPDX-License-Identifier: Apache-2.0
 * 
 * Direction Indicator (https://gitlab.com/itlernpfad_public/direction_indicator)
 * Copyright 2018 - 2021 The Direction Indicator Authors
 * Copyright 2020 - 2021 ITlernpfad
 * 
 * ============================================================================
 * ============================================================================
 * This software contains software derived from portions of Lightweight millisecond 
 * tracking library (led.c) by Zak Kemble, with various modifications by 
 * ITlernpfad and other Direction Indicator authors, see 
 * Direction Indicator AUTHORS file.
 * 
 * Lightweight millisecond tracking library (http://blog.zakkemble.net/millisecond-tracking-library-for-avr/)
 * Copyright 2018 Zak Kemble
 * Dual-licensed under the GNU General Public License, Version 3.0 or the MIT license.
 */

/*
 * @package Direction_Indicator
 * @file src/blinker.c
 * @authors 2018 - 2021 ITlernpfad and other Direction Indicator authors
 * (https://gitlab.com/ITlernpfad_Public/direction_indicator/AUTHORS)
 * @copyright 2018 - 2021 The Direction Indicator authors
 * Copyright 2020 - 2021 ITlernpfad
 * Copyright 2018 Zak Kemble
 * @brief Part of Direction Indicator
 * @details Direction Indicator blinks LEDs in the form of trailing lights. It is implemented as a state machine in non-blocking C.  
 * language: C99
 * status: Beta
 * @version 1.0.2
 */

/*
 * Project: Lightweight millisecond tracking library
 * Author: Zak Kemble, contact@zakkemble.net
 * Copyright: (C) 2018 by Zak Kemble
 * License: GNU GPL v3 or MIT (see LicenseRef-MIT-millis.txt)
 * Web: http://blog.zakkemble.net/millisecond-tracking-library-for-avr/
 */

/**
 * Firmware for the Atmega328P of an Arduino Uno Rev.3 with the following connections:
 *
 * Arduino Uno pin 8 -- push button (normally open) -- Arduino Uno 5V pin
 * Arduino Uno pin 8 -- 10kOhm resistor R5 -- Arduino Uno GND pin
 * Arduino Uno pin 9 -- LED1 (yellow) -- 220kOhm resistor R1 -- Arduino Uno GND pin
 * Arduino Uno pin 10 -- LED2 (yellow) -- 220kOhm resistor R2 -- Arduino Uno GND pin
 * Arduino Uno pin 11 -- LED3 (yellow) -- 220kOhm resistor R3 -- Arduino Uno GND pin
 * Arduino Uno pin 12 -- LED4 (yellow) -- 220kOhm resistor R4 -- Arduino Uno GND pin
 */ 

#include <avr/io.h>  /* https://www.nongnu.org/avr-libc/user-manual/group__avr__io.html . AVR device-specific IO definitions. */
#include <util/delay.h>  /* https://www.nongnu.org/avr-libc/user-manual/group__util__delay.html . Provides function _delay_ms */
#include <avr/interrupt.h>  /* https://nongnu.org/avr-libc/user-manual/group__avr__interrupts.html . Interrrupts */
#include <stdio.h>  /* https://www.nongnu.org/avr-libc/user-manual/group__avr__stdio.html . Provides function printf. */
#include <stdint.h>

/* https://gitlab.com/ITlernpfad_Public/stdio_setup . Provides function UartInit. For redirecting stdio stream to USB */
#include "stdio_setup.h"

/* https://github.com/zkemble/millis . Provides functions millis_init and millis. Uses Clear Timer on Compare (CTC) mode of the selected timer (default: timer 2) */
#include "millis.h"

/* define button state names as constants for better readability*/
#define RELEASED 0
#define PRESSED 1
#define DEBOUNCING 2

/* define blinker state names as constants for better readability*/
#define OFF_RELEASED 0
#define OFF_PRESSED_TO_BLINK 1
#define ON_RELEASED 2
#define OFF_PRESSED_TO_STOP 3

#define SWITCHING_INTERVAL 500UL
#define BUTTON_DEBOUNCE_INTERVAL 100UL


int main(void) {
    /* initialize millisecond timing. https://github.com/zkemble/millis  */
    millis_init();

    /* 
    Redirect stdio stream (output from function printf) to UART. 
    On Arduino Uno-compatible boards, UART data is sent to USB. https://gitlab.com/ITlernpfad_Public/stdio_setup
    */
    //UartInit();

    /* configure I/O pins */    
    DDRB &= ~(1U << 0U);  /* configure pin PB0 (Arduino Uno digital I/O pin 8, connected to button SW1) as input */

    DDRB |= (1U << 1U);  /* configure pin PB1 (Arduino Uno digital I/O pin 9, connected to LED1) as output */
    DDRB |= (1U << 2U);  /* configure pin PB2 (Arduino Uno digital I/O pin 10, connected to LED2) as output */
    DDRB |= (1U << 3U);  /* configure pin PB3 (Arduino Uno digital I/O pin 11, connected to LED3) as output */
    DDRB |= (1U << 4U);  /* configure pin PB0 (Arduino Uno digital I/O pin 12, connected to LED4) as output */

    PORTB &= ~(1U << 1U);  /* drive pin PB1 (Arduino Uno digital I/O pin 9) LOW to turn LED1 off */
    PORTB &= ~(1U << 2U);  /* drive pin PB2 (Arduino Uno digital I/O pin 10) LOW to turn LED2 off */
    PORTB &= ~(1U << 3U);  /* drive pin PB3 (Arduino Uno digital I/O pin 11) LOW to turn LED3 off */
    PORTB &= ~(1U << 4U);  /* drive pin PB4 (Arduino Uno digital I/O pin 12) LOW to turn LED4 off */

    /* variable for storing timestamp of last LED change */
    millis_t lastLedChangeMs = 0U;
    /* variable for storing timestamp of last change of button pin state */
    millis_t lastDebounceMs = 0U;

    /* state variables */
    uint8_t debouncedButtonState = RELEASED;
    int8_t blinkerState = OFF_RELEASED;

    /* raw button input pin status */
    uint8_t buttonPinState = (PINB >> 0U) & 1U; /* read bit 0 of register PINB to get state of pin PB0 */

    /* 
    Enable interrupts (https://nongnu.org/avr-libc/user-manual/group__avr__interrupts.html). 
    Required for millis library (https://github.com/zkemble/millis) 
    */
    sei();

    for(;;) {

        /* poll raw button input pin status and debounce */

        buttonPinState = (PINB >> 0U) & 1U; /* read bit 0 of register PINB to get state of pin PB0 */

        /* processing button finite state machine for debouncing */

        if (buttonPinState != debouncedButtonState) {
            debouncedButtonState = DEBOUNCING;
            printf("debouncedButtonState: DEBOUNCING");
            if ((millis() - lastDebounceMs) >= BUTTON_DEBOUNCE_INTERVAL) {
                /* button debounce time interval since last button pin state change is over, so take current button pin reading as the stable button state */
                if ((PINB >> 0U) & 1U) {
                    /* bit 0 of PINB register holds value 1, which means that pin PB0 is stable in HIGH state. Change button finite state machine to state PRESSED */ 
                    debouncedButtonState = PRESSED;
                } 
                else {
                    /* bit 0 of PINB register is stable at value 0, which means that pin PB0 is stable in LOW state. Change button finite state machine to state RELEASED */
                    debouncedButtonState = RELEASED;
                } 
                printf("buttonStateChange %u\n", debouncedButtonState);
                printf("buttonPinState: %u\n", buttonPinState);
                /* reset the debouncing timer */
                lastDebounceMs = millis();
            }
        }

        /* processing blinker finite state machine */

        if (blinkerState == OFF_RELEASED) {
            /* LEDs off, button released. Wait until button is pressed to start blinking LEDs. */
            printf("blinkerState: BLINKER_OFF\n");
            if (debouncedButtonState == PRESSED) {
                printf("buttonPinState: %u\n", buttonPinState);
                blinkerState = OFF_PRESSED_TO_BLINK;
            }
        }

        else if (blinkerState == OFF_PRESSED_TO_BLINK) {
            /* LEDs off, button pressed. Wait until button is released to start blinking LEDs. */
            printf("blinkerState: OFF_PRESSED_TO_BLINK. Wait for button release.\n");
            if (debouncedButtonState == 0) {
                blinkerState = ON_RELEASED;
            }
        }

        else if (blinkerState == ON_RELEASED) {
            /* LEDs blinking, button released. */
            printf("blinkerState: ON_RELEASED\n");

            /* Sequentially flash LEDs. */ 
            for (int8_t index = 1U; index <= 4U; index++) {

                if (debouncedButtonState == PRESSED) {
                    /* transition to blinker state OFF_PRESSED. */

                    /* turn off all LEDs by driving pins 1 to 4 of port B (Arduino Uno digital I/O pins 9 to 12) LOW */
                    PORTB &= ~(1U | 2U | 3U | 4U);
                    blinkerState = OFF_PRESSED_TO_STOP;

                    break;
                }

                /* wait until more milliseconds than SWITCHING_INTERVAL have elapsed since last change */
                if (millis() - lastLedChangeMs >= SWITCHING_INTERVAL) {
                    uint8_t ledBit = index + 1U;  /* loop index goes from 0 to 3, but LEDs are connected to port B pins 1 to 4 */

                    uint8_t previousLedBit;
                    if (ledBit == 1U) {
                        previousLedBit = 4U;
                    }
                    else {
                        previousLedBit = ledBit - 1;
                    }
     
                    PORTB &= ~(1U << previousLedBit);  /* clear previousLedBIT in register PORTB to drive the respective pin of port B LOW. This turns off the LED. */
                    printf("LED %u off\n", previousLedBit);
                    PORTB |= (1U << ledBit);  /* set ledBit in register PORTB to drive the respective pin of port B HIGH. This turns on the LED. */
                    printf("LED %u on\n", ledBit);

                    /* store current time as timestamp of change */
                    lastLedChangeMs = millis();
                }
            }
        }
        
        else if (blinkerState == OFF_PRESSED_TO_STOP) {
            /* LEDs off, button pressed. Wait for button release. */
            printf("blinkerState: PRESSED_TO_STOP. Wait for button release.\n");
            if (debouncedButtonState == RELEASED) {
                blinkerState = OFF_RELEASED;
            }
        }

    /* Above, the blinker finite state machine was processed. */

    }

    return 0;

}
