# Direction indicator
Firmware for a Direction indicator that föashes flashes LEDs in the form of trailing lights when a button is pressed and stops when the button is pressed again.
This firmware is written for the Atmega328P of an Arduino Uno Rev.3 with the following connections:

```
Arduino Uno pin 8 -> push button (normally open) -> Arduino Uno 5V pin
Arduino Uno pin 8 -> 10kOhm resistor R5 -> Arduino Uno GND pin
Arduino Uno pin 9 -> LED1 (yellow) -> 220kOhm resistor R1 -> Arduino Uno GND pin
Arduino Uno pin 10 -> LED2 (yellow) -> 220kOhm resistor R2 -> Arduino Uno GND pin
Arduino Uno pin 11 -> LED3 (yellow) -> 220kOhm resistor R3 -> Arduino Uno GND pin
Arduino Uno pin 12 -> LED4 (yellow) -> 220kOhm resistor R4 -> Arduino Uno GND pin
```

## How to build PlatformIO based project

1. Install the PlatformIO IDE (https://docs.platformio.org)
   1. [Install PlatformIO IDE for VSCode](https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode)
   2. [Install a Git client](https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode)
   3. Run Microsoft Visual Studio Code with the PlatformIO IDE. Left-click "File -> Preferences -> Settings -> Extensions". In section "Git", left-click on link to settings.json 
   4. in the file settings.json opened in step iii), verify if the entry for the key "git.path" specifies the correct path to the git executable. 
2. Install the Platform for the target
   1. Run Microsoft Visual Studio Code with the PlatformIO IDE. In the left VSCode Action toolbar, left-click "PlatformIO -> Quick Access -> Platforms -> Embedded". 
   2. Use the filter to search for "Atmel AVR". 
   3. Left-click on the platform "Atmel AVR -> Install".
   4. Wait until installation has succeeded and follow the instructions on screen. 
3. Clone Direction_Indicator 
   1. Run Microsoft Visual Studio Code with the PlatformIO IDE. In the left toolbar, left-click "PlatformIO -> Quick Access -> Miscellaneous -> Clone Git Project". 
   2. Type "https://gitlab.com/itlernpfad_public/direction_indicator" and press the enter-key. 
   3. Select a path for the local reposiory and confirm, then wait.
   4. In the notification at the bottom right, left-click "Add to Workspace".
4. Initiate the build process
   1. In VSCode, open the Explorer view: In the left VSCode Action toolbar, left-click "Explorer".
   2. In the Explorer panel of VSCode, left-click on the platformio.ini file at the root of the project "Direction_Indicator".
   3. Verify that platform.ini contains the correct content (for build target Arduino Uno Rev. 3, see below)
   4. In the PlatformIO toolbar at the bottom (<https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode-toolbar>), left-click on the central "Project Environment Switcher" and select "Default (atmel-avr-example-native)". 
   5. In the PlatformIO toolbar at the bottom, left-click on the button "PlatformIO: Build" (alternatively, in the left VSCode Actio toolbar, click "PlatformIO -> Project tasks -> General -> Default -> Build All).
   6. In the PlatformIO toolbar at the bottom, left-click on the button "PlatformIO: Build" (alternatively, in the left VSCode Actio toolbar, click "PlatformIO -> Project tasks -> General -> Default -> Upload All).

plaformio.ini for build target Arduino Uno Rev.3:
```
[platformio]
default_envs = uno
include_dir = src  

[env]
lib_deps      = 
  https://gitlab.com/itlernpfad_public/stdio_setup.git

[common_uno]
platform = atmelavr
board = uno

upload_protocol = arduino

[env:uno]
extends = common_uno
build_type = release
build_flags = 
; compiler options for linking in an implementation of the printf function that supports floating point conversions.
; https://www.nongnu.org/avr-libc/user-manual/group__avr__stdio.html
  -Wl,-u,vfprintf
  -lprintf_flt
  -lm
```

## Licensing information

```
Direction_Indicator (https://gitlab.com/itlernpfad_public/direction_indicator)
Copyright 2021 ITlernfad
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
